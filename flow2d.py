import math
from random import random
import scipy.linalg
import scipy.sparse
import scipy.sparse.linalg
import numpy as np
import matplotlib.pyplot as plt
#
# step (1.1) setup physical and computational parameters
#
nx    = int(480/4)                   # number of mesh cells in x
ny    = int(288/4)                   # number of mesh cells in y
lx    = 30./1                        # domain length in x
ly    = 18./1                        # domain length in y
dx    = lx/(1.0*nx)                  # mesh spacing in x
dy    = ly/(1.0*ny)                  # mesh spacing in y
uin   = 1.0                          # inflow velocity
uref  = uin                          # reference velocity scale (to calculate the viscosity from Reynolds number)
lref  = 2.*0.5                       # reference length scale   (to calculate the viscosity from Reynolds number)
rey   = 40.                          # Reynolds number
visc  = uref*lref/rey                # kinematic viscosity
dt    = 0.15*min(dx/uref,dx**2/visc) # time step size
nstep = 100000                       # maximum number of time steps
tmax  = 1000.                        # maximum physical time
small = 1.e-8                        # a small number
x_cyl = lx/3.                        # x position of the cylinder
y_cyl = ly/2.                        # y position of the cylinder
r_cyl = 0.5                          # cylinder radius
cyl = plt.Circle((x_cyl-x_cyl,y_cyl-y_cyl),r_cyl) # draw a circle
#
# define grids for plotting purposes
#   note that the grid is staggered:
#   pressure, x-velocity (u) and y-velocity (v)
#   are defined in different locations
#
xp  = np.arange(dx/2.,lx+dx/2.,dx)
yp  = np.arange(dy/2.,ly+dy/2.,dy)
xu  = np.arange(dx   ,lx+dx   ,dx)
yu  = np.arange(dy/2.,ly+dy/2.,dy)
xv  = np.arange(dx/2.,lx+dx/2.,dx)
yv  = np.arange(dy   ,ly+dy   ,dy)
XP,YP = np.meshgrid(xp,yp)
XP = np.transpose(XP)
YP = np.transpose(YP)
XU,YU = np.meshgrid(xu,yu)
XU = np.transpose(XU)
YU = np.transpose(YU)
XV,YV = np.meshgrid(xv,yv)
XV = np.transpose(XV)
YV = np.transpose(YV)
#
# step (1.2) setup and initialize field arrays
#   the two extra grid points that are defined
#   (i.e. [nx+2,ny+2] and not [nx,ny])
#   correspond to auxiliary cells that are required
#   for imposing boundary conditions
#
u   = np.zeros([nx+2,ny+2]) # x velocity component
v   = np.zeros([nx+2,ny+2]) # y velocity component
p   = np.zeros([nx+2,ny+2]) # pressure
pp  = np.zeros([nx+2,ny+2]) # pressure
up  = np.zeros([nx+2,ny+2]) # x-component of the prediction velocity
vp  = np.zeros([nx+2,ny+2]) # y-component of the prediction velocity
fx  = np.zeros([nx+2,ny+2]) # x-component of the IBM force
fy  = np.zeros([nx+2,ny+2]) # y-component of the IBM force
#
rhs = np.zeros([nx+2,ny+2]) # rhs of the Poisson equation
#
# step (1.3) initialize Poisson solver matrix, linear system Mx = b
#
# Poisson equation: (p[i-1,j]-2p[i,j]-p[i-1,j])/dx**2 + (p[i,j+1]-2p[i,j]-p[i,j-1])/dy**2 = RHS[i,j]
# where RHS[i,j] = div(vel)[i,j]/dt = (dudx[i,j] + dvdy[i,j])/dt
#
# so the Poisson matrix will be (excluding boundary conditions):
#
# for q = 1 to nx*ny:
#
# M[q   ,q] = (-2./dx**2 -2./dy**2) | i  ,j terms
# M[q+1 ,q] =  1/dy**2              | j+1,i terms
# M[q-1 ,q] =  1/dy**2              | j-1,i terms
# M[q+ny,q] =  1/dx**2              | j,i+1 terms
# M[q-ny,q] =  1/dx**2              | j,i-1 terms
# the termaining terms in M are zero
#
# boundary conditions (BC):
# dpdx = 0 in x = 0  ! velocity imposed -> zero pressure correction
# dpdy = 0 in y = 0  ! velocity imposed -> zero pressure correction
# dpdy = 0 in y = ly ! velocity imposed -> zero pressure correction
# p    = 0 in x = lx ! zero pressure outflow
#
# example for BC dpdy = 0 at y = 0:
#
# p[i-1,1]-2p[i,1]-p[i-1,1])/dx**2 + (p[i,2]-2p[i,1]-p[i,0])/dy**2 = RHS[i,1]
#
# note that the pressure is not defined at the domain boundary since we use a
# staggered grid. It is defined in the cells just before and
# just after the boundary. So we have for the pressure gradient at x = 0:
#
# dpdy[i,0] = 0 -> (p[i,1]-p[i,0])/dx = 0 -> p[i,1] = p[i,0]
#
# hence, the Poisson equation will become (substitute p[i,0]):
#
# p[i-1,1]-2p[i,1]-p[i-1,1])/dx**2 + (p[i,2]-2p[i,1]+p[i,1])/dy**2 = RHS[i,1] <->
# p[i-1,1]-2p[i,1]-p[i-1,1])/dx**2 + (p[i,2]- p[i,1]       )/dy**2 = RHS[i,1]
# so the coefficient should be modified at the boundary to impose this BC
#
# for a constant pressure BC, p=0 at x = lx, the procedure is analogous, but using
# (p[nx+1,j]+p[nx,j])/2 = 0 -> p[nx,j] = -p[nx+1,j]
#
n = nx*ny                    # number unknowns in the Poisson equation
b = np.zeros([nx*ny      ])  # right-hand-side vector
x = np.zeros([nx*ny      ])  # vector of unknowns
q  = 0
qq = 0
data    = np.zeros(n*5) # matrix element
rows    = np.zeros(n*5) # row index
cols    = np.zeros(n*5) # column index
#
for i in range(1,nx+1):
    for j in range(1,ny+1):
        aay =  1./dy**2 # (i,j-1) element of the stencil
        ccy =  1./dy**2 # (i,j+1) element of the stencil
        aax =  1./dx**2 # (i-1,j) element of the stencil
        ccx =  1./dx**2 # (i+1,j) element of the stencil
        bb  = -(aax+ccx+aay+ccy)
        if(j == 1 ): # Neumann (zero gradient) boundary condition for the pressure at y = 0
            bb += aay
            aay = 0.
        if(j == ny): # Neumann (zero gradient) boundary condition for the pressure at y = ly
            bb += ccy
            ccy = 0.
        if(i == 1 ): # Neumann (zero gradient) boundary condition for the pressure at x = 0
            bb += aax
            aax = 0.
        if(i == nx): # Dirichlet (zero value)  boundary condition for the pressure at x = lx
            bb -= ccx
            ccx = 0.
        #
        # fill in the matrix
        #
        data[qq] = bb # diagonal
        rows[qq] = q
        cols[qq] = q
        if( q > 0    ):
            data[qq+1] = aay # lower diagonal corresponding to i,j-1
            rows[qq+1] = q
            cols[qq+1] = q-1
        if( q < n-1  ):
            data[qq+2] = ccy # upper diagonal corresponding to i,j+1
            rows[qq+2] = q
            cols[qq+2] = q+1
        if( q > ny-1 ):
            data[qq+3] = aax # lower diagonal corresponding to i-1,j
            rows[qq+3] = q
            cols[qq+3] = q-ny
        if( q < n-ny ):
            data[qq+4] = ccx # upper diagonal corresponding to i+1,j
            rows[qq+4] = q
            cols[qq+4] = q+ny
        q  += 1
        qq += 1*5
rows=rows.astype(int)
cols=cols.astype(int)
MM = scipy.sparse.csc_matrix((data,(rows,cols)),shape=(n,n))
#plt.spy(MM,ms=0.5)
#plt.show()
#
# LU decomposition of the Poisson matrix
#
LU = scipy.sparse.linalg.splu(MM)
#
# step (1.4) setup the initial conditions
# (here all the variables inside the domain are initialized as zero
#  but that is not always the case!)
#
u[1:nx+1,:] = 0.
v[1:nx+1,:] = 0.
p[1:nx+1,:] = 0.
##
##   uncomment the for loop below to add some small noise to the flow
##   (e.g. to trigger the vortex shedding for
##    the flow around the cylinder faster)
##
#for i in range(1,nx+1):
#    for j in range(1,ny+1):
#        u[i,j] = uin + 0.*random()
#        v[i,j] = 0.  + 0.005*random()
#
# step (1.5) define boundary conditions
#   here:
#       at x = 0 , for all y: u      = uin, v    = 0, dpdx = 0
#       at x = lx, for all y: div(u) = 0  , dvdx = 0, p    = 0
#       at y = 0 , for all x: dudy   = 0  , v    = 0, dpdx = 0
#       at y = ly, for all x: dudy   = 0  , v    = 0, dpdx = 0
#
# left   boundary ( x = 0  ):
u[0  ,:] = uin
v[0  ,:] = 0.*2. - v[1,:]   # -> v[0,:] = -v[1,:]
p[0  ,:] = 0.*dx + p[1,:]   # -> p[0,:] =  p[1,:]
# right  boundary ( x = lx ):
u[nx+1,:] = u[nx-1,:]       # -> dummy BC
u[nx,: ]  = u[nx-1,:]
v[nx,:]   = 0.*dx + v[nx-1,:] # -> v[n,:] =  v[n-1,:]
p[nx+1,:] = 0.*2. - p[nx,:] # -> p[n+1,:] = -p[n  ,:]
# bottom boundary ( y = 0  )
u[:,0] = 0.*dy + u[:,1]     # -> u[:,0] = u[:,1]
v[:,0] = 0.
p[:,0] = 0.*dy + p[:,1]     # -> p[:,0] = p[:,1]
# top    boundary ( y = ly ):
u[:,ny+1] = 0.*dy + u[:,ny] # -> u[:,n+1] = u[:,n]
v[:,ny  ] = 0.
v[:,ny+1] = v[:,ny-1]
p[:,ny+1] = 0.*dy + p[:,ny] # -> p[:,n+1] = p[:,n]
#
pp[:,:] = p[:,:]
#
# some preliminary calls for plotting purposes
#
fig1, ax = plt.subplots()
w, h = plt.figaspect(ly/lx)
fig1.set_size_inches(w,h)
fig, axes = plt.subplots(2, 1)
#
# step (2) time advancement
#
done  = False
istep = 0
time  = 0.
arr_forces = np.array([[0.,0.,0.]])
while not done:
    istep += 1
    time  += dt
    print('Time step number: %6.6i, physical time t = %15.7E' %(istep,time))
    if( istep >= nstep or time >= tmax ): done = True # check simulation ending criteria
    #
    # step (2.1) compute prediction velocities, up,vp,wp (u* in the notes)
    #
    up[:,:] = 0.
    vp[:,:] = 0.
    for i in range(1,nx+1):
        for j in range(1,ny+1):
            #
            # x-component of the prediction velocity
            #
            up[i,j] += u[i,j]
            #
            # (a) compute comtribution from the advection terms
            #
            uup = (0.5*(u[i+1,j  ] + u[i  ,j  ]))*(0.5*(u[i+1,j  ] + u[i  ,j  ]))
            uum = (0.5*(u[i  ,j  ] + u[i-1,j  ]))*(0.5*(u[i  ,j  ] + u[i-1,j  ]))
            uvp = (0.5*(u[i  ,j+1] + u[i  ,j  ]))*(0.5*(v[i+1,j  ] + v[i  ,j  ]))
            uvm = (0.5*(u[i  ,j  ] + u[i  ,j-1]))*(0.5*(v[i+1,j-1] + v[i  ,j-1]))
            up[i,j] += -( (uup-uum)/dx + (uvp-uvm)/dy )*dt
            #
            # (b) compute contribution from the diffusion (viscous) terms
            #
            dudxp = (u[i+1,j]-u[i  ,j])/dx
            dudxm = (u[i  ,j]-u[i-1,j])/dx
            dudyp = (u[i,j+1]-u[i,j  ])/dy
            dudym = (u[i,j  ]-u[i,j-1])/dy
            up[i,j] += visc*((dudxp-dudxm)/dx + (dudyp-dudym)/dy)*dt
            #
            # (c) compute contribution from the pressure gradient term
            #
            dpdx = -(p[i+1,j]-p[i,j])/dx
            up[i,j] += dpdx*dt
            #
            # y-component of the prediction velocity
            #
            vp[i,j] += v[i,j]
            #
            # (a) compute comtribution from the advection terms
            #
            vup = 0. # COMPLETE!
            vum = 0. # COMPLETE!
            vvp = 0. # COMPLETE!
            vvm = 0. # COMPLETE!
            vp[i,j] += -( (vup-vum)/dx + (vvp-vvm)/dy )*dt
            #
            # (b) compute contribution from the diffusion (viscous) terms
            #
            dvdxp = 0. # COMPLETE!
            dvdxm = 0. # COMPLETE!
            dvdyp = 0. # COMPLETE!
            dvdym = 0. # COMPLETE!
            vp[i,j] += visc*((dvdxp-dvdxm)/dx + (dvdyp-dvdym)/dy)*dt
            #
            # (c) compute contribution from the pressure gradient term
            #
            dpdy = -(p[i,j+1]-p[i,j])/dy
            vp[i,j] += dpdy*dt
    #
    # impose boundary conditions on the prediction velocity
    #
    # left   boundary ( x = 0  ):
    up[0   ,:] = uin
    vp[0   ,:] = 0.*2. - vp[1,:] # -> v[0,:] = -v[1,:]
    # right  boundary ( x = lx ):
    up[nx+1,:] = up[nx-1,:]       # -> dummy BC
    up[nx  ,:] = up[nx-1,:]       # -> dummy BC
    vp[nx,:] = 0.*dx + vp[nx-1,:] # -> v[n+1,:] = v[n,:]
    # bottom boundary ( y = 0  ):
    up[:   ,0] = 0.*dy + up[:,1]   # -> u[:,0] = u[:,1]
    vp[:   ,0] = 0.
    # top    boundary ( y = ly ):
    up[:,ny+1] = 0.*dy + up[:,ny] # -> u[:,n+1] = u[:,n]
    vp[:,ny+1] = vp[:,ny-1]
    vp[:,ny  ] = 0.
    #
    # Immersed-Boundary forcing a la Fadlun et al, JCP 2000
    #
    # (do not try to understand this part now :), it is an efficient way to accomodate
    # a cylinder inside the domain)
    #
    fx[:,:] = 0.
    fy[:,:] = 0.
    for i in range(1,nx+1):
        x_p = i*dx - dx/2. # x-coordinate of the pressure   cell
        x_u = x_p + dx/2.  # x-coordinate of the x-velocity cell
        x_v = x_p          # x-coordinate of the y-velocity cell
        for j in range(1,ny+1):
            y_p = j*dy - dy/2. # y-coordinate of the pressure   cell
            y_u = y_p          # y-coordinate of the x-velocity cell
            y_v = y_p + dy/2.  # y-coordinate of the y-velocity cell
            sdist_u_c = np.sqrt((x_u   -x_cyl)**2 + (y_u   -y_cyl)**2) - r_cyl # signed distance to the cylinder for u[i  ,j]
            sdist_u_l = np.sqrt((x_u-dx-x_cyl)**2 + (y_u   -y_cyl)**2) - r_cyl # signed distance to the cylinder for u[i-1,j]
            sdist_u_u = np.sqrt((x_u+dx-x_cyl)**2 + (y_u   -y_cyl)**2) - r_cyl # signed distance to the cylinder for u[i+1,j]
            sdist_v_c = np.sqrt((x_v   -x_cyl)**2 + (y_v   -y_cyl)**2) - r_cyl # signed distance to the cylinder for v[i,j  ]
            sdist_v_l = np.sqrt((x_v   -x_cyl)**2 + (y_v-dy-y_cyl)**2) - r_cyl # signed distance to the cylinder for v[i,j-1]
            sdist_v_u = np.sqrt((x_v   -x_cyl)**2 + (y_v+dy-y_cyl)**2) - r_cyl # signed distance to the cylinder for v[i,j+1]
            if(sdist_u_c > 0. and sdist_u_l*sdist_u_u < 0.): # if the grid cell is outside the cylinder, and close to the surface
                if(  sdist_u_l < 0. ):
                    sdist_u = sdist_u_l
                    idir = +1
                elif(sdist_u_u < 0. ):
                    sdist_u = sdist_u_u
                    idir = -1
                u_cyl   = 0.             # cylinder velocity
                x_i = x_cyl + idir*np.sqrt(r_cyl**2-(y_u-y_cyl)**2)
                u_aux   = up[i+idir,j]   # auxiliary velocity at grid point i+idir,j
                x_u_aux = x_u + idir*dx  # x-coordinate of auxiliary velocity grid point i+idir,j
                u_f   = ((u_aux-u_cyl)/(x_u_aux-x_i))*(x_u-x_i) # desired velocity
                fx[i,j] = (u_f - up[i,j])/dt
            if(sdist_v_c > 0 and sdist_v_l*sdist_v_u < 0.): # if the grid cell is outside the cylinder, and close to the surface
                if( sdist_v_l < 0.):
                    sdist_v = sdist_v_l
                    jdir = +1
                elif(sdist_v_u < 0. ):
                    sdist_v = sdist_v_u
                    jdir = -1
                v_cyl   = 0.             # cylinder velocity
                y_i = y_cyl + jdir*np.sqrt(r_cyl**2-(x_v-x_cyl)**2)
                v_aux   = vp[i,j+jdir]   # auxiliary velocity at grid point j+jdir,i
                y_v_aux = y_v + jdir*dy  # x-coordinate of auxiliary velocity grid point j+jdir,i
                v_f   = ((v_aux-v_cyl)/(y_v_aux-y_i))*(y_v-y_i) # desired velocity
                fy[i,j] = (v_f - vp[i,j])/dt
    up[:,:] += fx[:,:]*dt # add IBM force to the prediction velocity
    vp[:,:] += fy[:,:]*dt # add IBM force to the prediction velocity
    #
    # (2.2) fill in the right hand side of the Poisson equation onto array b
    #
    q = 0
    for i in range(1,nx+1):
        for j in range(1,ny+1):
            div = 0. # COMPLETE!
            rhs[i,j] = div/dt # RHS of the Poisson equation
            b[q] = rhs[i,j]
            q += 1
    #
    # (2.3) solve the pressure Poisson equation; here we use a (slow) direct solution
    #
    x = LU.solve(b)
    q = 0
    for i in range(1,nx+1):
        for j in range(1,ny+1):
            pp[i,j] = x[q]
            q += 1
    #
    # impose boundary conditions on the correction pressure field
    #
    # left   boundary ( x = 0  ):
    pp[0  ,:] = 0.*dx + pp[1,:]   # -> p[0,:] =  p[1,:]
    # right  boundary ( x = lx ):
    pp[nx+1,:] = 0.*2. - pp[nx,:] # -> p[n+1,:] = -p[n,:]
    # bottom boundary ( y = 0  ):
    pp[:,0] = 0.*dx + pp[:,1]     # -> p[:,0] = p[:,1]
    # top    boundary ( y = ly ):
    pp[:,ny+1] = 0.*dx + pp[:,ny] # -> p[:,n+1] = p[:,n]
    #
    # (2.4) pressure correction step --
    # correct the velocity field with the pressure gradient so that it is divergence-free
    #
    for i in range(1,nx+1):
        for j in range(1,ny+1):
            u[i,j] = up[i,j] - dt*(pp[i+1,j]-pp[i,j])/dx
            v[i,j] = vp[i,j] # COMPLETE!
    #
    # compute final pressure
    # (note that the pressure is decomposed into p_new = p_old + pp)
    # to improve the temporal accuracy of the method
    #
    p[:,:] += pp[:,:]
    #
    # boundary conditions for the final pressure and velocity
    #
    # left   boundary ( x = 0  ):
    u[0  ,:] = uin
    v[0  ,:] = 0.*2. - v[1,:] # -> v[0,:] = -v[1,:]
    p[0  ,:] = 0.*dx + p[1,:] # -> p[0,:] =  p[1,:]
    # right  boundary ( x = lx ):
    u[nx+1,1:ny+2] = u[nx,1:ny+2] - dx*((v[nx+1,1:ny+2]-v[nx+1,0:ny+1])/dy)
    v[nx,:  ] = 0.*dx + v[nx-1,:] # -> v[n+1,:] = v[n,:]
    p[nx+1,:] = 0.*2. - p[nx  ,:] # -> p[n+1,:] = -p[n,:]
    # bottom boundary ( y = 0  ):
    u[:,0] = 0.*dy + u[:,1]   # -> u[:,0] = -u[:,1]
    v[:,0] = 0.
    p[:,0] = 0.*dy + p[:,1]   # -> p[:,0] = p[:,1]
    # top    boundary ( y = ly ):
    u[:,ny+1] = 0.*dy + u[:,ny] # -> u[:,n+1] = -u[:,n]
    v[:,ny+1] = v[:,ny-1]
    v[:,ny  ] = 0.
    p[:,ny+1] = 0.*dy + p[:,ny] # -> p[:,n+1] = -u[:,n]
    #
    # finally, some very simple post-processing
    #
    # compute lift and drag coefficients
    #
    drag = np.sum(fx[:,:]*dx*dy)/(0.5*uin**2*2.*r_cyl) # drag coefficient
    lift = np.sum(fy[:,:]*dx*dy)/(0.5*uin**2*2.*r_cyl) # lift coefficient
    arr_forces = np.append(arr_forces,[[time,drag,lift]],axis=0)
    print(time,lift,drag)
    if (istep % 10 == 0 ):
        # plot lift and drag
        axes[0].plot(arr_forces[9:,0],-arr_forces[9:,1],'-r')
        axes[0].set_xlabel('')
        axes[0].set_ylabel('drag coefficient')
        axes[1].plot(arr_forces[9:,0],arr_forces[9:,2],'-b')
        axes[1].set_xlabel('time')
        axes[1].set_ylabel('lift coefficient')
        axes[0].set_xlim([max([0.,time-10.]),time])
        axes[0].set_ylim([0,3.])
        axes[1].set_xlim([max([0.,time-10.]),time])
        axes[1].set_ylim([-.1,.1])
        plt.setp(axes[0].get_xticklabels(), visible=False)
        fig.canvas.draw_idle()
        plt.pause(0.01)
    if (istep % 10 == 0 ):
        #vorticity = np.zeros([nx+2,ny+2])
        for i in range(1,nx+1):
            for j in range(1,ny+1):
                div = (u[i,j]-u[i-1,j])/dx + (v[i,j]-v[i,j-1])/dy
                if(abs(div) >= small ): print("Divergence too large! div = ", div)
                #vorticity[i,j] = 0.5*((v[i+1,j]-v[i-1,j])/(2.*dx) + (v[i+1,j-1]-v[i-1,j-1])/(2.*dx)) - \
                #                 0.5*((u[i,j+1]-u[i,j-1])/(2.*dy) + (u[i-1,j+1]-u[i-1,j-1])/(2.*dy))
        uc = np.zeros([nx+2,ny+2])
        vc = np.zeros([nx+2,ny+2])
        uc[1:nx+1,:] = 0.5*(u[0:nx,:]+u[1:nx+1,:])
        vc[:,1:ny+1] = 0.5*(v[:,0:ny]+v[:,1:ny+1])
        unorm = np.sqrt(uc**2+vc**2)
        umax  = np.max(unorm)
        dt = 0.15*min(dx/umax,dx**2/visc) # time step size
        cnt = ax.contourf(XP-x_cyl,YP-y_cyl,unorm[1:nx+1,1:ny+1],levels=40)
        skp = max([1,int(ly/dy/20)])
        ax.quiver(XP[::skp,::skp]-x_cyl,YP[::skp,::skp]-y_cyl,uc[1:nx+1:skp,1:ny+1:skp],vc[1:nx+1:skp,1:ny+1:skp])
        ax.add_artist(cyl)
        ax.set_xlim([0.-x_cyl,lx-x_cyl])
        ax.set_ylim([0.-y_cyl,ly-y_cyl])
        ax.set_title('velocity magnitude')
        #if(istep == 10 ): cb = fig1.colorbar(cnt,ax=ax)
        fig1.canvas.draw_idle()
        plt.pause(0.01)
